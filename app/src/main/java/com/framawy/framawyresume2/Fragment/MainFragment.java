package com.framawy.framawyresume2.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.framawy.framawyresume2.Classes.MainStrings;
import com.framawy.framawyresume2.R;
import com.github.florent37.viewanimator.ViewAnimator;

public class MainFragment extends Fragment {

    MainStrings fragmentStrings = new MainStrings();

    public MainFragment() {    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ImageView avatar = view.findViewById(R.id.avatar);
        TextView maintxt = view.findViewById(R.id.maintxt);
        View lineview = view.findViewById(R.id.lineview);

        RequestOptions option = new RequestOptions().fitCenter().circleCrop();
        Glide.with(this).load(R.drawable.avatar_small).apply(option).into(avatar);


        ViewAnimator
                .animate(lineview)
                .waitForHeight() //wait until a ViewTreeObserver notification
                .dp().width(40,250)
                .dp().height(3,3)
                .duration(1000)
                .start();

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            maintxt.setText(fragmentStrings.stringsMainFragment());
            YoYo.with(Techniques.StandUp)
                    .duration(1000)
                    .playOn(maintxt);

        }, 1200);

        return view;
    }
}