package com.framawy.framawyresume2.Classes;

public class ProfileStrings {
    String birth_date = "Data of birth : 28/3/1997";
    String social_statues = "Social statues : Single";
    String nationality = "Nationality : Egyptian";

    String arabic  = "Arabic  : Mother tongue";
    String english = "English : Fluent";

    String[] hobbies = {"Coffee", "Music", "Movies", "Reading", "Travelling"};

    public ProfileStrings() {
    }

    public StringBuilder stringsProfileFragment() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("• ")
                .append(birth_date)
                .append("\n\n").append("• ")
                .append(social_statues)
                .append("\n\n").append("• ")
                .append(nationality);


        return stringBuilder;
    }

    public StringBuilder stringslanguages() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("• ")
                .append(arabic)
                .append("\n\n").append("• ")
                .append(english);


        return stringBuilder;
    }


    public StringBuilder stringsHobbies() {
        StringBuilder stringBuilder2 = new StringBuilder();

        for (String i : hobbies) {
            stringBuilder2
                    .append("• ")
                    .append(i)
                    .append("\n\n");
        }


        return stringBuilder2;
    }
}
