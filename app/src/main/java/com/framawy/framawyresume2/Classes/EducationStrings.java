package com.framawy.framawyresume2.Classes;

 public class EducationStrings {
    String high_school = "International Liberty School American Diploma - Graduate 2015";
    String college = "AAST Computer Engineering - 2015 - 2020";
    String courses = "java OOP New Horizon";

    String[] skills = {"Android java" ,"OOP", "XML" , "JSON" , "Experience with 3rd libraries" , "Git", "Firabase" , "Microsoft office",
    "Software engineering concepts" , "Debugging", "Writing documentations", "Photoshop & Illustrator"};



    public EducationStrings() {
    }

     public StringBuilder stringsEducationFragment() {
         StringBuilder stringBuilder = new StringBuilder();
         stringBuilder
                 .append("• ")
                 .append(high_school)
                 .append("\n").append("• ")
                 .append(college)
                 .append("\n").append("• ")
                 .append(courses);


         return stringBuilder;
     }


     public StringBuilder stringsSkills() {
         StringBuilder stringBuilder2 = new StringBuilder();

         for (String i : skills) {
             stringBuilder2
                     .append("• ")
                     .append(i)
                     .append("\n");
         }


         return stringBuilder2;
     }
}
