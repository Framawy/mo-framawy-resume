package com.framawy.framawyresume2.Fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.framawy.framawyresume2.Classes.ProfileStrings;
import com.framawy.framawyresume2.R;

public class ProfileFragment extends Fragment {

    ProfileStrings profileStrings;
    TextView personal_info;
    TextView languages_txt;
    TextView hobbies_txt;
    ConstraintLayout parent;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        profileStrings = new ProfileStrings();

        personal_info = view.findViewById(R.id.personalinfo_txt);
        languages_txt = view.findViewById(R.id.languagestxt);
        hobbies_txt = view.findViewById(R.id.hobbiestxt);
        parent = view.findViewById(R.id.profile_parent);

        personal_info.setText(profileStrings.stringsProfileFragment());
        hobbies_txt.setText(profileStrings.stringsHobbies());
        languages_txt.setText(profileStrings.stringslanguages());

        return view;
    }




    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }
}