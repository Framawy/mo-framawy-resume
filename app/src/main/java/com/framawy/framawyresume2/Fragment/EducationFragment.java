package com.framawy.framawyresume2.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.framawy.framawyresume2.Classes.EducationStrings;
import com.framawy.framawyresume2.R;


public class EducationFragment extends Fragment {



    public EducationFragment() {    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_education, container, false);

        EducationStrings educationStrings = new EducationStrings();
        TextView educationtxt = view.findViewById(R.id.educationtxt);
        TextView skillstxt = view.findViewById(R.id.skillstxt);

        educationtxt.setText(educationStrings.stringsEducationFragment());
        skillstxt.setText(educationStrings.stringsSkills());

        return view;
    }
}