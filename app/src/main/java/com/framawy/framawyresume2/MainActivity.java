package com.framawy.framawyresume2;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.framawy.framawyresume2.Classes.SectionsPagerAdapter;
import me.kungfucat.viewpagertransformers.RotateAboutTopTransformer;

public class MainActivity extends AppCompatActivity {

    int[] colors = {R.color.main, R.color.profile, R.color.education, R.color.contact_me};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());

        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);


        LinearLayout toolbar = findViewById(R.id.main_toolbar);

        TextView title = findViewById(R.id.title);
        title.setText(sectionsPagerAdapter.getPageTitle(viewPager.getCurrentItem()));

        TextView pagenum = findViewById(R.id.pageNUM);
        pagenum.setText("¹/₄");
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                title.setText(sectionsPagerAdapter.getPageTitle(position));
                String index = superscript(position) + "/₄";
                pagenum.setText(index);
                getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), colors[position]));
                toolbar.setBackgroundResource(colors[position]);

                ConstraintLayout profile_parent = findViewById(R.id.profile_parent);
                ConstraintLayout education_parent = findViewById(R.id.education_parent);
                ConstraintLayout contact_parent = findViewById(R.id.contact_parent);

                //Fragment Views animation is done from the main activity because the viewpager load the fragment with its
                //animation even if fragment is not visible yet
                switch (position) {
                    case 0:
                        YoYo.with(Techniques.StandUp).duration(2000).playOn(findViewById(R.id.maintxt));
                        break;
                    case 1:
                        profile_parent.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.FadeInUp).duration(2000).playOn(profile_parent);
                        break;
                    case 2:
                        education_parent.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.FadeInLeft).duration(2000).playOn(education_parent);
                        break;
                    case 3:
                        contact_parent.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.RotateInDownRight).duration(2000).playOn(contact_parent);
                        break;


                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setPageTransformer(true, new RotateAboutTopTransformer());


    }

    //UTF code of superscripted numbers
    public static String superscript(int x) {
        String s = null;
        switch (x) {
            case 0:
                s = "¹";
                break;
            case 1:
                s = "²";
                break;
            case 2:
                s = "³";
                break;
            case 3:
                s = "⁴";
                break;
        }

        return s;

    }
}