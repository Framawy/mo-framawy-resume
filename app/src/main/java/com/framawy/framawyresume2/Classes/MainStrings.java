package com.framawy.framawyresume2.Classes;


import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;

public class MainStrings {

    SpannableString name = new SpannableString("Mohammed Ahmed ElFramawy");
    SpannableString work_statues = new SpannableString("Junior Android developer");
    int flag = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;

    public MainStrings() {
        name.setSpan(new StyleSpan(Typeface.BOLD), 0, name.length(), flag);
        work_statues.setSpan(new StyleSpan(Typeface.ITALIC), 0, work_statues.length(), flag);
    }

    public SpannableStringBuilder  stringsMainFragment() {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        stringBuilder.append(name).append("\n\n").append(work_statues);

        return stringBuilder;
    }
}
