package com.framawy.framawyresume2.Classes;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.framawy.framawyresume2.Fragment.ContactFragment;
import com.framawy.framawyresume2.Fragment.MainFragment;
import com.framawy.framawyresume2.Fragment.ProfileFragment;
import com.framawy.framawyresume2.Fragment.EducationFragment;


public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private static final String[] TAB_TITLES = new String[]{"Resume", "Profile" , "Education & Skills"  , "Contact me"};

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0 : return new MainFragment();
            case 1 : return new ProfileFragment();
            case 2 : return new EducationFragment();
            case 3 : return new ContactFragment();
        }
        return null;

    }

    @Nullable
    @Override
    public String getPageTitle(int position) {
        return TAB_TITLES[position];
    }

    @Override
    public int getCount() { return 4; }
}